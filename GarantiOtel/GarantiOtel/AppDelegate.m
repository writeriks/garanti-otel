//
//  AppDelegate.m
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "AppDelegate.h"
#import "InvoicesVC.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application
  didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
     UIImage *navBackgroundImage = [UIImage imageNamed:@"turuncu-buton.png"];
    [[UINavigationBar appearance] setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [self customAppearence];
    [self userExist];
    return YES;
}

-(void)userExist{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *segueID1 = @"sth";
    NSString *segueID2 = @"...";
    
    InvoicesVC *login = [storyboard instantiateViewControllerWithIdentifier:segueID1];
    
    if (![defaults boolForKey:@"registered"]) {
        NSLog(@"It is not a toy ya GangsTa playing with");
    }
    else{
        NSLog(@"EVERYBODY GETS DA PAArTY!!");
        [self.window setRootViewController:login];
    }
}

-(void)customAppearence {
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, -1000) forBarMetrics:UIBarMetricsDefault];
    ///////// NAVIGATION BAR
    UIImage *backBtn = [UIImage imageNamed:@"geri-butonu1.png"];
    backBtn = [backBtn imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [[UINavigationBar appearance]setBackIndicatorImage:backBtn];
    [[UINavigationBar appearance]setBackIndicatorTransitionMaskImage:backBtn];
    ///////// NAVIGATION BAR
    
    ///////// SEARCH BAR
    [[UISearchBar appearance] setTintColor:[UIColor blackColor]];
    //////// SEARCH BAR
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
