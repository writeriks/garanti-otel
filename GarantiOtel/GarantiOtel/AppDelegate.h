//
//  AppDelegate.h
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

