//
//  FAQVC.m
//  GarantiOtel
//
//  Created by volkan.terzioglu on 12/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "FAQVC.h"

#import "GOService.h"
@interface FAQVC ()<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic) NSArray * FAQs;

@property (strong, nonatomic) IBOutlet UILabel *question;
@property (strong, nonatomic) IBOutlet UILabel *answer;

@end

@implementation FAQVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getFAQ];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self designVC];
}


-(void)designVC{
    /// TABLEVIEW BACKGROUND IMAGE ATAMA
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
    [tempImageView setFrame:self.tableView.frame];
    self.tableView.backgroundView = tempImageView;
    ///
    //self.tableView.separatorColor = [UIColor clearColor];
}
-(void)getFAQ{

    [[GOService sharedService] getFAQOfUser:nil
                                     result:^(NSArray *array, NSError *error) {
                                         if (array.count != 0) {
                                             self.FAQs = array;
                                             [self.tableView reloadData];
                                         }
                                         else {
                                         // yok
//                                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hata" message:@"SSS getirilemedi" delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil, nil];
//                                             
//                                             
//                                             alert.show;
                                             NSLog(@"FAQ yok!");
                                         }
    }];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.FAQs.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"faqCell"];
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"faqCell"];
    }
    FAQ *currentFAQ = [self.FAQs objectAtIndex:indexPath.row];
    
    cell.textLabel.text = currentFAQ.soru;
    cell.detailTextLabel.text = currentFAQ.cevap;
    cell.textLabel.textColor = [UIColor blackColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0f;
}



@end
