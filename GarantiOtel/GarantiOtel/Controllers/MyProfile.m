//
//  MyProfile.m
//  GarantiOtel
//
//  Created by volkan.terzioglu on 12/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "MyProfile.h"

@interface MyProfile ()<UITextFieldDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate>

@end
BOOL keyboardIsShown; 
@implementation MyProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    [self insideViewDidLoad];
    [self keyboardNotifications];
    keyboardIsShown = NO;
    CGSize scrollContentSize = CGSizeMake(320, 345);
    self.scrollView.contentSize = scrollContentSize;
    [self.txtMail setDelegate:self];
}

-(void)insideViewDidLoad{
    self.txtMail.delegate = self;
    self.txtNameSurname.delegate = self;
    
    [self.txtNameSurname setBackgroundColor:[UIColor clearColor]];
    [self.txtMail setBackgroundColor:[UIColor clearColor]];
    [self.lblNameSurname setBackgroundColor:[UIColor clearColor]];
    [self.lblMail setBackgroundColor:[UIColor clearColor]];
    [self.txtNameSurname setHidden:YES];
    [self.txtMail   setHidden:YES];
    self.txtNameSurname.text = self.lblNameSurname.text;
    self.txtMail.text = self.lblMail.text;
    self.imgProfilePic.layer.cornerRadius = 10;
    self.imgProfilePic.clipsToBounds = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.lblNameSurname.text = [defaults objectForKey:@"username"];
    self.lblMail.text = [defaults objectForKey:@"email"];
    self.txtNameSurname.text = [defaults objectForKey:@"username"];
    self.txtMail.text = [defaults objectForKey:@"email"];
}
-(void)keyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height += (keyboardSize.height); //- kTabBarHeight);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

#pragma -mark keyboardShowMethods
- (void)keyboardWillShow:(NSNotification *)n
{
    // This is an ivar I'm using to ensure that we do not do the frame size adjustment on the `UIScrollView` if the keyboard is already shown.  This can happen if the user, after fixing editing a `UITextField`, scrolls the resized `UIScrollView` to another `UITextField` and attempts to edit the next `UITextField`.  If we were to resize the `UIScrollView` again, it would be disastrous.  NOTE: The keyboard notification will fire even when the keyboard is already shown.
    if (keyboardIsShown) {
        return;
    }
    NSDictionary* userInfo = [n userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    viewFrame.size.height -= (keyboardSize.height);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}

#pragma -mark Textfield Klavye Kapa
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnChangeProfilePic:(id)sender {

}
- (IBAction)editNameSurnameButtonTapped:(id)sender {
   [self.lblNameSurname setHidden:YES];
   [self.txtNameSurname setHidden:NO];
   [self.txtNameSurname becomeFirstResponder];
}

- (IBAction)editMailButtonTapped:(id)sender {
   [self.lblMail setHidden:YES];
   [self.txtMail setHidden:NO];
   [self.txtMail becomeFirstResponder];
}

- (IBAction)editProfilePic:(id)sender {
    UIActionSheet *actionsheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"iptal" destructiveButtonTitle:nil otherButtonTitles:@"Fotoğraf Çek",@"Kütüphaneden yükle", nil];
    [actionsheet showInView:self.view];
}
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0 ) {
        [self openCamera];
    }
    if (buttonIndex == 1 ) {
        [self selectPhoto];
    }
}

#pragma mark - camera and library methods
-(void)openCamera{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertView *noCamera= [[UIAlertView alloc]initWithTitle:@"Hata!" message:@"Kamera Bulunamadı." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
        [noCamera show];
    }
    else{
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.navigationBar.tintColor = [UIColor blackColor];
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)selectPhoto{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.navigationBar.tintColor = [UIColor blackColor];
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imgProfilePic.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma  -mark Button Save Action Method
- (IBAction)saveProfileButtonTapped:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.txtNameSurname.text forKey:@"username"];
    [defaults setObject:self.txtMail.text forKey:@"email"];
    [defaults synchronize];
}



- (IBAction)resignFirstResponderTapped:(id)sender {
    [self.txtMail resignFirstResponder];
    [self.txtNameSurname resignFirstResponder];
}
@end
