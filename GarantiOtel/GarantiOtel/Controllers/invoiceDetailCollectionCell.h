//
//  invoiceDetailCollectionCell.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 24/03/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface invoiceDetailCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *invoiceDetailImageView;

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end
