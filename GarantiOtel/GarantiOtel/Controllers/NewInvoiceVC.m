    //
    //  NewInvoiceVC.m
    //  GarantiOtel
    //
    //  Created by Emir haktan Ozturk on 01/02/15.
    //  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
    //

#import "NewInvoiceVC.h"
#import "GOService.h"
#import "InvoiceCollectionCell.h"
#import "imageVC.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "InvoicePost.h"
#import "InvoicePicture.h"
#import <CoreGraphics/CoreGraphics.h>

    @interface NewInvoiceVC ()<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,NSURLConnectionDelegate,NSURLConnectionDataDelegate>{
    MBProgressHUD *HUD;
}
    - (IBAction)cancelButton:(id)sender;
    - (IBAction)saveButtonTapped:(id)sender;
    @end

    @implementation NewInvoiceVC

    - (void)viewDidLoad {
        [super viewDidLoad];
        [self designVC];
        [self datePickerSettings];
}

    -(void)datePickerSettings{
    self.datePicker = [[UIDatePicker alloc]init];   
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    [self.txtSonOdemeTarihi setInputView:self.datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc]initWithTitle:@"Ekle" style:UIBarButtonItemStyleDone target:self action:@selector(showSelectedDate)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn,nil]animated:YES];
    [self.txtSonOdemeTarihi setInputAccessoryView:toolBar];
}

    - (IBAction)firstResponderResignationTapped:(id)sender {
    [self.txtFaturaAdi resignFirstResponder];
    [self.txtFaturaNo resignFirstResponder];
    [self.txtSonOdemeTarihi resignFirstResponder];
}

    -(void)showSelectedDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.txtSonOdemeTarihi.text = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:self.datePicker.date]];
    [self.txtSonOdemeTarihi resignFirstResponder];
}

    -(void)designVC{
    self.txtFaturaNo.delegate = self;
    self.txtFaturaAdi.delegate = self;
    self.invoiceCollectionView.delegate = self;
//  self.invoiceCollectionViewBackgroundImage.backgroundColor = [UIColor clearColor];
//  self.invoiceCollectionView.backgroundColor = [UIColor clearColor];
//  self.txtSonOdemeTarihi.delegate = self;
    self.invoiceCollectionViewBackgroundImage.image = [UIImage imageNamed:@"faturalar-köşeli-buton.png"];
    self.invoicePhotoArray = [[NSMutableArray alloc] init];
    [self.saveButtonOutlet setBackgroundColor:[UIColor clearColor]];
    [self.deleteButtonOutlet setBackgroundColor:[UIColor clearColor]];
    [self.txtFaturaAdi setBackgroundColor:[UIColor clearColor]];
    [self.txtFaturaNo setBackgroundColor:[UIColor clearColor]];
    [self.txtSonOdemeTarihi setBackgroundColor:[UIColor clearColor]];
}

    -(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

    #pragma mark - ActionSheet Methods
    - (IBAction)buttonTakePhoto:(id)sender {
    UIActionSheet *actionsheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"iptal" destructiveButtonTitle:nil otherButtonTitles:@"Fotoğraf Çek",@"Kütüphaneden yükle", nil];
    [actionsheet showInView:self.view];
    }

    -(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0 ) {
    [self openCamera];
    }
    if (buttonIndex == 1 ) {
    [self selectPhoto];
    }
}

    #pragma mark - camera and library methods imagePickerController
    -(void)openCamera{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    UIAlertView *noCamera= [[UIAlertView alloc]initWithTitle:@"HATA" message:@"Kamera Bulunamadı." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [noCamera show];
    }
    else{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.navigationBar.tintColor = [UIColor blackColor];
    [self presentViewController:picker animated:YES completion:NULL];
    }
}

    -(void)selectPhoto{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.navigationBar.tintColor = [UIColor blackColor];
    [self presentViewController:picker animated:YES completion:NULL];
    }

    -(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self.invoicePhotoArray addObject:chosenImage];
    [self.invoiceCollectionView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

    -(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}



    #pragma mark collectionview Delegate methods
    -(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
    -(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.invoicePhotoArray count];
}

    -(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    InvoiceCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"invoiceCollectionCell" forIndexPath:indexPath];
    cell.invoiceImageView.image = [self.invoicePhotoArray objectAtIndex:indexPath.row];
    cell.invoiceImageView.layer.cornerRadius = 10;
    cell.invoiceImageView.clipsToBounds = YES;
    return cell;
}

    -(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.invoiceCollectionView.bounds.size.width/4, self.invoiceCollectionView.bounds.size.height - 10);
}

    -(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
        InvoiceCollectionCell *cell = [[InvoiceCollectionCell alloc] init];
        float center = self.invoiceCollectionView.bounds.size.width/2 - self.invoiceCollectionView.bounds.size.width / 8;
    if (self.invoicePhotoArray.count ==1) {
    return UIEdgeInsetsMake(0, center, 0, 0);
}
    else{
    return UIEdgeInsetsMake(0, 0, 0, 0);
    }

}
    -(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

    #pragma mark CollectionViewCell Delete Button Action
    - (IBAction)deleteButton:(UIButton *)sender {
    NSIndexPath *indexpath = [self.invoiceCollectionView indexPathForCell:sender.superview.superview];
    [self.invoicePhotoArray removeObjectAtIndex:indexpath.row];
    [self.invoiceCollectionView reloadData];
}

    #pragma  mark - prepare for imageSegue
    -(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.destinationViewController  isKindOfClass:[imageVC class]]) {
        NSIndexPath *indexPath = [self.invoiceCollectionView indexPathForCell:sender];
        UIImage *currentImage = [self.invoicePhotoArray objectAtIndex:indexPath.row];
        imageVC *destination = segue.destinationViewController;
        destination.image = currentImage;
    }
}

    #pragma mark cancel method
    - (IBAction)cancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

    #pragma mark button save
    - (IBAction)saveButtonTapped:(id)sender {
        //yükleme HUD
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.labelText = @"Yükleniyor";
        HUD.detailsLabelText = @"Lütfen Bekleyin";
        HUD.animationType = MBProgressHUDAnimationZoomOut;
        [self.view addSubview:HUD];
        [HUD showWhileExecuting:@selector(postString) onTarget:self withObject:nil animated:YES];
}


    #pragma mark string post method
    -(void)postString{
    InvoicePost *invPost = [[InvoicePost alloc] init];
    invPost.faturaNo = @"123";
    // Date Conversion
    NSString *str = self.txtSonOdemeTarihi.text; /// here this is your date with format dd-MM-yyyy
    NSLog(@"%@",str);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ; // here we create NSDateFormatter object for change the Format of date..
    [dateFormatter setDateFormat:@"dd-MM-yyyy"]; //// here set format of date which is in your output date (means above str with format)
    NSDate *date = [dateFormatter dateFromString: str]; // here you can fetch date from string with define format
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];// here set format which you want...
    NSString *convertedString = [dateFormatter stringFromDate:date];
    NSLog(@"Converted String : %@",convertedString);
    CFTimeInterval startDate = [date timeIntervalSince1970] * 1000;
    NSString *dateStarted = [NSString stringWithFormat:@"/Date(%.0f+0800)/", startDate];
    //
    invPost.faturaTarihi = dateStarted;
    invPost.faturaTutari = @"12321";
    invPost.ID = @"232";
    invPost.kategoriID = @"2321";
    invPost.kayitTarihi = dateStarted;
    invPost.onay = @"true";
    invPost.saticiFirma = @"emirHolding";
    invPost.uyeID = @"343";
    NSString * returnString = [[GOService sharedService]newInvoiceWithUser:nil andID:invPost.ID andInvoiceNo:invPost.faturaNo andDealerFirm:invPost.saticiFirma andMemberID:invPost.uyeID andInvoiceAmount:invPost.faturaTutari andInvoiceDate:invPost.faturaTarihi andCategoryID:invPost.kategoriID andRegistryDate:invPost.kayitTarihi andPermission:invPost.onay];
    NSLog(returnString);
    [self postImageData:returnString];
}


    #pragma mark image post method
    -(void)postImageData:registryID{
    NSMutableArray *array = [[NSMutableArray alloc]init];
      //  for (int i = 0; i< self.invoicePhotoArray.count; i++) {
    UIImage *image = [UIImage imageNamed:@"buton.png"];//self.invoicePhotoArray[i];
    NSData *dosya = UIImagePNGRepresentation(image);
    const unsigned char *bytes = [dosya bytes];
    NSUInteger length = [dosya length];
    NSMutableArray *byteArray = [NSMutableArray array];
    for (NSUInteger i = 0; i < length; i++) {
        [byteArray addObject:[NSNumber numberWithUnsignedChar:bytes[i]]];
    }
      //[array addObject:byteArray];
//}
    InvoicePicture *invPic = [[InvoicePicture alloc] init];
    invPic.aciklama = @"aciklama";
    invPic.dosyaAdi = @"resim";
    invPic.dosyaBoyutu = @"123";
    invPic.dosyaTipi = @"emir";
    invPic.dosyaUzantisi = @"emiremir";
    invPic.ID = @"345" ;
    invPic.kayitID = registryID;
    CFTimeInterval startDate = [[NSDate date] timeIntervalSince1970] * 1000;
    NSString *dateStarted = [NSString stringWithFormat:@"/Date(%.0f+0800)/", startDate];
    invPic.kayitZamani = dateStarted;
    invPic.onay = @"true";
    invPic.personelID = @"32423";
    invPic.profil = @"true";
    invPic.sirasi = @"3242";
    invPic.tabloAdi = @"resimler";
    NSString *result = [[GOService sharedService]newInvoiceWithImageArray:byteArray andID:invPic.ID andDescription:invPic.aciklama andFileName:invPic.dosyaAdi andFileLength:invPic.dosyaBoyutu andFileType:invPic.dosyaTipi andFileExtention:invPic.dosyaUzantisi andRegistryID:invPic.kayitID andRegistryDate:invPic.kayitZamani andPermission:invPic.onay andPersonalID:invPic.personelID andProfile:invPic.profil andSequence:invPic.sirasi andTableName:invPic.tabloAdi];
    if ([result isEqualToString:@"{\"UyeFaturaResimKaydetResult\":true}"]) {
        NSLog(@"OK");
        [HUD setHidden:YES];
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.labelText = @"Yükleme Başarılı";
        HUD.mode = MBProgressHUDModeText;
        HUD.animationType = MBProgressHUDAnimationZoomOut;
        [self.view addSubview:HUD];
        [HUD showWhileExecuting:@selector(timeLong) onTarget:self withObject:nil animated:YES];
    }
        else
        [HUD setHidden:YES];
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.labelText = @"Yükleme Başarısız";
        HUD.mode = MBProgressHUDModeText;
        HUD.animationType = MBProgressHUDAnimationZoomOut;
        [self.view addSubview:HUD];
        [HUD showWhileExecuting:@selector(timeLong) onTarget:self withObject:nil animated:YES];

}

    -(void)timeLong {
    [NSThread sleepForTimeInterval:1.0f];
}





///// Buradan sonrası komple silinecek...
    -(void)postData{
    InvoicePost *invPost = [[InvoicePost alloc] init];
    invPost.faturaNo = @"123";
    CFTimeInterval startDate = [[NSDate date] timeIntervalSince1970] * 1000;
    NSString *dateStarted = [NSString stringWithFormat:@"/Date(%.0f+0800)/", startDate];
    invPost.faturaTarihi = dateStarted;
    invPost.faturaTutari = @"12321";
    invPost.ID = @"232";
    invPost.kategoriID = @"2321";
    invPost.kayitTarihi = dateStarted;
    invPost.onay = @"true";
    invPost.saticiFirma = @"emirHolding";
    invPost.uyeID = @"343";
    NSArray *keys = [NSArray arrayWithObjects:@"faturaNo",@"faturaTarihi",@"faturaTutari",@"ID",@"kategoriID",@"kayitTarihi",@"onay",@"saticiFirma",@"uyeID" , nil];
    NSArray *objects = [NSArray arrayWithObjects:invPost.faturaNo,invPost.faturaTarihi,invPost.faturaTutari,invPost.ID,invPost.kategoriID,invPost.kayitTarihi,invPost.onay,invPost.saticiFirma,invPost.uyeID, nil];
    NSData *_jsonData=nil;
    NSString *_jsonString=nil;
    NSURL *url2=[NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/UyeFaturaKaydet"];
    NSDictionary *JsonDictionary=[NSDictionary dictionaryWithObjects:objects forKeys:keys];
    if([NSJSONSerialization isValidJSONObject:JsonDictionary]){
   _jsonData=[NSJSONSerialization dataWithJSONObject:JsonDictionary options:0 error:nil];
   _jsonString=[[NSString alloc]initWithData:_jsonData encoding:NSUTF8StringEncoding];
    }
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url2];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:_jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[_jsonData length]] forHTTPHeaderField:@"Content-Length"];
    NSHTTPURLResponse *response = nil;
    NSError *error = [[NSError alloc] init];
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", data);
    if (error == nil) {
    NSLog(@"%@",urlData);
    }
    [self photoUploadWithID:data];
    
    
//    NSArray *keys=[NSArray arrayWithObjects:@"prm1",@"prm2",nil];
//    NSArray *objects=[NSArray arrayWithObjects:@"prm1Cevap",@"prm2Cevap",nil];
//
//    NSData *_jsonData=nil;
//    NSString *_jsonString=nil;
//
//    NSURL *url2=[NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/postmethod"];
//    NSDictionary *JsonDictionary=[NSDictionary dictionaryWithObjects:objects forKeys:keys];
//
//
//    if([NSJSONSerialization isValidJSONObject:JsonDictionary]){
//
//    _jsonData=[NSJSONSerialization dataWithJSONObject:JsonDictionary options:0 error:nil];
//    _jsonString=[[NSString alloc]initWithData:_jsonData encoding:NSUTF8StringEncoding];
//    }
//
//    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url2];
//    [request setHTTPMethod:@"POST"];
//    [request setHTTPBody:_jsonData];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[_jsonData length]] forHTTPHeaderField:@"Content-Length"];
//
//    NSHTTPURLResponse *response = nil;
//    NSError *error = [[NSError alloc] init];
//    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", data);
//    if (error == nil) {
//    //        return urlData;
//    NSLog(@"%@",urlData);
//    }
}

    -(void)photoUploadWithID:picData{
    //resim çevirme
    NSMutableArray *array = [[NSMutableArray alloc]init];
//  for (int i = 0; i< self.invoicePhotoArray.count; i++) {
    UIImage *image = [UIImage imageNamed:@"buton.png"];
    NSData *dosya = UIImagePNGRepresentation(image);
    const unsigned char *bytes = [dosya bytes]; // no need to copy the data
    NSUInteger length = [dosya length];
    NSMutableArray *byteArray = [NSMutableArray array];
    for (NSUInteger i = 0; i < length; i++) {
    [byteArray addObject:[NSNumber numberWithUnsignedChar:bytes[i]]];
    }
//  [array addObject:byteArray];
//  }
    InvoicePicture *invPic = [[InvoicePicture alloc] init];
    invPic.aciklama = @"aciklama";
    invPic.dosyaAdi = @"resim";
    invPic.dosyaBoyutu = @"123";
    invPic.dosyaTipi = @"emir";
    invPic.dosyaUzantisi = @"emiremir";
    invPic.ID = @"345" ;
    invPic.kayitID = picData;
    CFTimeInterval startDate = [[NSDate date] timeIntervalSince1970] * 1000;
    NSString *dateStarted = [NSString stringWithFormat:@"/Date(%.0f+0800)/", startDate];
    invPic.kayitZamani = dateStarted;
    invPic.onay = @"true";
    invPic.personelID = @"32423";
    invPic.profil = @"true";
    invPic.sirasi = @"3242";
    invPic.tabloAdi = @"resimler";
    NSArray *keyForInvPic = [NSArray arrayWithObjects:@"aciklama",@"dosyaAdi",@"dosyaBoyutu",@"dosyaTipi",@"dosyaUzantisi",@"ID",@"kayitID",@"kayitZamani",@"onay",@"personelID",@"profil",@"sirasi",@"tabloAdi",nil];
    NSArray *objectForInvPic = [NSArray arrayWithObjects:invPic.aciklama,invPic.dosyaAdi,invPic.dosyaBoyutu,invPic.dosyaTipi,invPic.dosyaUzantisi,invPic.ID,invPic.kayitID,invPic.kayitZamani,invPic.onay,invPic.personelID,invPic.profil,invPic.sirasi,invPic.tabloAdi, nil];
    NSData *_jsonData2=nil;
    NSString *_jsonString2=nil;
    NSDictionary *JsonDictionary2=[NSDictionary dictionaryWithObjects:objectForInvPic forKeys:keyForInvPic];
    if([NSJSONSerialization isValidJSONObject:JsonDictionary2]){
    _jsonData2=[NSJSONSerialization dataWithJSONObject:JsonDictionary2 options:0 error:nil];
    _jsonString2=[[NSString alloc]initWithData:_jsonData2 encoding:NSUTF8StringEncoding];
    }
    NSArray *keys=[NSArray arrayWithObjects:@"faturaResim",@"dosya",nil];
    NSArray *objects=[NSArray arrayWithObjects:JsonDictionary2,byteArray,nil];
    NSData *_jsonData=nil;
    NSString *_jsonString=nil;
    NSDictionary *JsonDictionary=[NSDictionary dictionaryWithObjects:objects forKeys:keys];
    if([NSJSONSerialization isValidJSONObject:JsonDictionary]){
    _jsonData=[NSJSONSerialization dataWithJSONObject:JsonDictionary options:0 error:nil];
    _jsonString=[[NSString alloc]initWithData:_jsonData encoding:NSUTF8StringEncoding];
    }
    NSURL *url=[NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/UyeFaturaResimKaydet"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:_jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[_jsonData length]] forHTTPHeaderField:@"Content-Length"];
    NSLog(@"REQUEST  %@",request);
    NSHTTPURLResponse *response = nil;
    NSError *error = [[NSError alloc] init];
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", data);
    if (error == nil) {
        NSLog(@"%@",urlData);
    }
    if ([data isEqualToString:@"{\"UyeFaturaResimKaydetResult\":true}"]) {
    NSLog(@"OK");
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Yükleme Başarılı";
    HUD.mode = MBProgressHUDModeText;
    HUD.animationType = MBProgressHUDAnimationZoomOut;
    [self.view addSubview:HUD];
    [HUD showWhileExecuting:@selector(timeLong) onTarget:self withObject:nil animated:YES];
    }
}
//-(void)uploadPhoto{
//    
//    
//    NSMutableArray *array = [[NSMutableArray alloc]init];
//
//
//    for (int i = 0; i< self.invoicePhotoArray.count; i++) {
//     
//    UIImage *image = self.invoicePhotoArray[i];
//
//    NSData *dosya = UIImagePNGRepresentation(image);
//        
//    const unsigned char *bytes = [dosya bytes]; // no need to copy the data
//    NSUInteger length = [dosya length];
//    NSMutableArray *byteArray = [NSMutableArray array];
//    for (NSUInteger i = 0; i < length; i++) {
//    [byteArray addObject:[NSNumber numberWithUnsignedChar:bytes[i]]];
//    }
// 
//        [array addObject:byteArray];
//        }
//
//            
//    NSArray *keys=[NSArray arrayWithObjects:@"text",@"dosya",nil];
//    NSArray *objects=[NSArray arrayWithObjects:@"textCevap",array,nil];
//    NSData *_jsonData=nil;
//    NSString *_jsonString=nil;
//    NSDictionary *JsonDictionary=[NSDictionary dictionaryWithObjects:objects forKeys:keys];
//    if([NSJSONSerialization isValidJSONObject:JsonDictionary]){
//
//    _jsonData=[NSJSONSerialization dataWithJSONObject:JsonDictionary options:0 error:nil];
//    _jsonString=[[NSString alloc]initWithData:_jsonData encoding:NSUTF8StringEncoding];
//    }
//
//    NSURL *url=[NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/testImage"];
//
//
//    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
//   [request setHTTPMethod:@"POST"];
//
//    [request setHTTPBody:_jsonData];
//
//
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[_jsonData length]] forHTTPHeaderField:@"Content-Length"];
//
//    NSLog(@"REQUEST  %@",request);
//
//    NSHTTPURLResponse *response = nil;
//    NSError *error = [[NSError alloc] init];
//
//    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
//
//    NSLog(@"%@", data);
//    if (error == nil) {
//    //        return urlData;
//    NSLog(@"%@",urlData);
//    }
//
//    if ([data isEqualToString:@"{\"testImageResult\":true}"]) {
//    NSLog(@"OK");
//        
//        HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        HUD.labelText = @"Yükleme Başarılı";
//        HUD.mode = MBProgressHUDModeText;
//        HUD.animationType = MBProgressHUDAnimationZoomOut;
//        [self.view addSubview:HUD];
//        
//        [HUD showWhileExecuting:@selector(timeLong) onTarget:self withObject:nil animated:YES];
//}
//
//}
//-(void)timeLong {
//    [NSThread sleepForTimeInterval:1.0f];
//}

    @end

