//
//  OurService.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 17/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"

@interface ServiceFirmsVC : GOViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic)BOOL isFiltered;
@end
