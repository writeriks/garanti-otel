//
//  OurService.m
//  GarantiOtel
//
//  Created by volkan.terzioglu on 17/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "ServiceFirmsVC.h"
#import "GOService.h"
@interface ServiceFirmsVC ()<UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate>

@property(strong, nonatomic) IBOutlet UITableView *tableview;
@property(nonatomic) NSMutableArray *serviceFirms;
@property(nonatomic) NSMutableArray *filteredServiceFirms;
@end

@implementation ServiceFirmsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    [self designVC];
    [self getServiceFirms];
    // Do any additional setup after loading the view.
}

-(void)designVC{

    UIImage *image = [UIImage imageNamed:@"background.png"];
    self.tableview.backgroundColor =[UIColor colorWithPatternImage:image];
    self.searchBar.barTintColor = [UIColor orangeColor];
    self.searchBar.translucent = NO;
    self.searchBar.barStyle = UIBarStyleDefault;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getServiceFirms{

            [[GOService sharedService] getServisFirmsOfUser:nil
                                                     result:^(NSArray *array, NSError *error) {
                                                         if (array.count != 0) {
                                                             self.serviceFirms = array;
                                                             [self.tableview reloadData ];
                                                         }
                                                         else{
//                                                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hata" message:@"Servis firmaları bulunamadı" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                                                             alert.show;
//                                                         // servis firm yok!
                                                         
                                                         NSLog(@"Service firm yok");
                                                         }
                                                     }];

}

#pragma mark tableview Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.serviceFirms.count;
    }

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceFirmsCell" ];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ServiceFirmsCell"];
    }
    ServiceFirms *currentSF = [self.serviceFirms objectAtIndex:indexPath.row];
    cell.textLabel.text = currentSF.firmaAdi;
    
    return  cell;
}

#pragma UISearchBar Methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    
    if (searchText.length ==0) {
        self.isFiltered = NO;
    }
    
    else{
        self.isFiltered = YES;
        self.filteredServiceFirms = [[NSMutableArray alloc] init];
        
        
        for (NSString *s in self.serviceFirms) {
            NSRange sRange = [s rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (sRange.location !=NSNotFound) {
                [self.filteredServiceFirms addObject:s];
            }
        }
        
        
    }

    [self.tableview reloadData];///////////EKSİKLER VAR SEARCHBAR A BAKILACAK
    
    
}



@end
