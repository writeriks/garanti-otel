//
//  InvoicesDetailVC.m
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "InvoiceDetailVC.h"
#import "GOService.h"
#import "invoiceDetailImageVC.h"
#import "invoiceDetailCollectionCell.h"
#import "Reachability.h"
BOOL keyboardIsShown;
@interface InvoiceDetailVC ()<UITextFieldDelegate,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>
{
    Reachability *reachabilityTest;
}
@property(nonatomic) NSArray * invoices;

@end

@implementation InvoiceDetailVC
//@synthesize invNo,invAmount,invFirmName;
//@synthesize invoiceAmount,invoiceFirmName,invoiceNo,invoicePicture;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testConnection];
    [self designVC];
    [self keyboardNotification];
    [self getDetails];
    keyboardIsShown = NO;
    CGSize scrollContentSize = CGSizeMake(320, 345);
    self.scrollview.contentSize = scrollContentSize;
    [self.invoiceAmount setDelegate:self];
    //[self getInvoiceDetails];
    [self.invoiceDetailCV reloadData];
}

-(void)testConnection{
    reachabilityTest = [Reachability reachabilityWithHostName:@"www.google.com"];
    // Internet is reachable
    reachabilityTest.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Yayyy, we have the interwebs!");
        });
    };
    
    // Internet is not reachable
    reachabilityTest.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Someone broke the internet :(");
        });
    };
    [reachabilityTest startNotifier];
}

-(void)designVC{
    
    self.title = self.inv.bankaSubeAdi;
    self.invoiceNo.delegate= self;
    self.invoiceFirmName.delegate = self;
    self.invoiceAmount.delegate = self;
    
    [self.invoiceNo setBackgroundColor:[UIColor clearColor]];
    [self.invoiceFirmName setBackgroundColor:[UIColor clearColor]];
    [self.invoiceAmount setBackgroundColor:[UIColor clearColor]];
    [self.lblInvoiceNo setBackgroundColor:[UIColor clearColor]];
    [self.lblInvoiceFirmName setBackgroundColor:[UIColor clearColor]];
    [self.lblInvoiceAmount setBackgroundColor:[UIColor clearColor]];
    
    [self.invoiceNo setHidden:YES];
    [self.invoiceFirmName setHidden:YES];
    [self.invoiceAmount setHidden:YES];
    
    self.invoiceNo.text = self.lblInvoiceNo.text;
    self.invoiceFirmName.text = self.lblInvoiceFirmName.text;
    self.invoiceAmount.text = self.lblInvoiceAmount.text;
    
    self.invoiceDetailPhotoArray = [[NSMutableArray alloc] init];
    //////////////////////////////////////////////BU BÖLGE SİLİNECEK
    [self.invoiceDetailPhotoArray addObject:[UIImage imageNamed:@"fotoğraf-ekleme-butonu.png"]];
    [self.invoiceDetailPhotoArray addObject:[UIImage imageNamed:@"image001.png"]];
    [self.invoiceDetailPhotoArray addObject:[UIImage imageNamed:@"image001.png"]];
    [self.invoiceDetailPhotoArray addObject:[UIImage imageNamed:@"image001.png"]];
    ///////////    ///////////    ///////////    ///////////    ///////////
    
    self.invoiceDetailCV.delegate = self;
    [self.invoiceNo setBackgroundColor:[UIColor clearColor]];
    [self.invoiceFirmName setBackgroundColor:[UIColor clearColor]];
    [self.invoiceAmount setBackgroundColor:[UIColor clearColor]];
    [self.btnEditInvoiceOutlet setBackgroundColor:[UIColor clearColor]];
}

-(void)keyboardNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
}

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    // resize the scrollview
    CGRect viewFrame = self.scrollview.frame;
    // I'm also subtracting a constant kTabBarHeight because my UIScrollView was offset by the UITabBar so really only the portion of the keyboard that is leftover pass the UITabBar is obscuring my UIScrollView.
    viewFrame.size.height += (keyboardSize.height); //- kTabBarHeight);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollview setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = NO;
}


- (void)keyboardWillShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    NSDictionary* userInfo = [n userInfo];
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect viewFrame = self.scrollview.frame;
    viewFrame.size.height -= (keyboardSize.height); // - kTabBarHeight);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.scrollview setFrame:viewFrame];
    [UIView commitAnimations];
    keyboardIsShown = YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

//-(void)getInvoiceDetails {
//    self.title = self.inv.saticiFirma;
//    self.invoiceNo.text = self.inv.faturaNo;
//    self.invoiceFirmName.text = self.inv.saticiFirma;
//    //self.invoiceAmount.text = self.inv.faturaTutari;
//}


-(void)getDetails{//Silinecek
    self.title = self.inv.bankaSubeAdi;
    self.invoiceFirmName.text = self.inv.bankaAdi;
    self.invoiceNo.text = [NSString stringWithFormat:@"%ld", (long)self.inv.ID];
    self.invoiceAmount.text = self.inv.bankaIBAN;
    self.lblInvoiceAmount.text =self.inv.bankaIBAN;
    self.lblInvoiceFirmName.text =self.inv.bankaAdi;
    self.lblInvoiceNo.text =[NSString stringWithFormat:@"%ld", (long)self.inv.ID];
}

#pragma -mark Label kapa Textfield Aç texfield klavye aç
- (IBAction)editInvoiceNoButtonTapped:(id)sender {
    [self.lblInvoiceNo setHidden:YES];
    [self.invoiceNo setHidden:NO];
    [self.invoiceNo becomeFirstResponder];
}

- (IBAction)editInvoiceFirmNameButtonTapped:(id)sender{
    [self.lblInvoiceFirmName setHidden:YES];
    [self.invoiceFirmName setHidden:NO];
    [self.invoiceFirmName becomeFirstResponder];
}
- (IBAction)editInvoiceAmountButtonTapped:(id)sender{
    [self.lblInvoiceAmount setHidden:YES];
    [self.invoiceAmount setHidden:NO];
    [self.invoiceAmount becomeFirstResponder];
}

#pragma mark Collection View Delegate Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.invoiceDetailPhotoArray count];
}

-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    invoiceDetailCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"invoiceDetailCollectionCell" forIndexPath:indexPath];
    cell.invoiceDetailImageView.image = [self.invoiceDetailPhotoArray objectAtIndex:indexPath.row];
    cell.invoiceDetailImageView.layer.cornerRadius = 10;
    cell.invoiceDetailImageView.clipsToBounds = YES;
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.invoiceDetailCV.bounds.size.width/4, self.invoiceDetailCV.bounds.size.height - 10);//90,100
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    float center = self.invoiceDetailCV.bounds.size.width/2-self.invoiceDetailCV.bounds.size.width / 8;//önceki hali 100 dü resmi ortada göstermek için
    if (self.invoiceDetailPhotoArray.count ==1) {
        return UIEdgeInsetsMake(0, center, 0, 0);
    }
    else{
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

#pragma -mark ADD PICTURE
- (IBAction)addImageButtonTapped:(id)sender {
    UIActionSheet *actionsheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"iptal" destructiveButtonTitle:nil otherButtonTitles:@"Fotoğraf Çek",@"Kütüphaneden yükle", nil];
    [actionsheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0 ) {
        [self openCamera];
    }
    if (buttonIndex == 1 ) {
        [self selectPhoto];
    }
}

#pragma -mark - camera and library methods UIimagePickerController
-(void)openCamera{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertView *noCamera= [[UIAlertView alloc]initWithTitle:@"Error" message:@"Kamera Bulunamadı." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [noCamera show];
    }
    else{
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.navigationBar.tintColor = [UIColor blackColor];
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)selectPhoto{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    picker.navigationBar.tintColor = [UIColor blackColor];
    picker.navigationBarHidden = YES;
    picker.toolbarHidden = YES;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self.invoiceDetailPhotoArray addObject:chosenImage];
    [self.invoiceDetailCV reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma -mark Delete Picture
- (IBAction)deleteButtonTapped:(UIButton *)sender{
    NSIndexPath *indexPath = [self.invoiceDetailCV indexPathForCell:sender.superview.superview];
    [self.invoiceDetailPhotoArray removeObjectAtIndex:indexPath.row];
    [self.invoiceDetailCV reloadData];  
}

#pragma  -mark Picture Detail
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.destinationViewController  isKindOfClass:[invoiceDetailImageVC class]]) {
        NSIndexPath *indexPath = [self.invoiceDetailCV indexPathForCell:sender];
        UIImage *currentImage = [self.invoiceDetailPhotoArray objectAtIndex:indexPath.row];
        invoiceDetailImageVC *destination = segue.destinationViewController;
        destination.image = currentImage;
    }
}

- (IBAction)resignFirstResponderTapped:(id)sender {
    [self.invoiceNo resignFirstResponder];
    [self.invoiceFirmName resignFirstResponder];
    [self.invoiceAmount resignFirstResponder];
}

- (IBAction)saveButtonTapped:(id)sender {
    [self testConnection];
}
@end
