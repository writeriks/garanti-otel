//
//  InvoicesVC.h
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"
#import "Invoice.h"

@interface InvoicesVC : GOViewController <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate>


@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) Invoice *invoice;


@end

