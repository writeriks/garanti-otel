//
//  SlideVC.m
//  GarantiOtel
//
//  Created by volkan.terzioglu on 11/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "SlideVC.h"

@interface SlideVC ()

@end

@implementation SlideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
}

-(NSString *) segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath
{
    NSString *identifier;
    switch (indexPath.row) {

        case 0:
            identifier = @"InvoicesSegue";
            break;
        case 1:
            identifier = @"MyProfileSegue";
            break;
        case 2:
            identifier = @"FAQSegue";
            break;
        case 3:
            identifier = @"ContactSegue";
            break;
        case 4:
            identifier = @"ServiceFirmsSegue";
            break;
        case 5:
            [self dismissViewControllerAnimated:YES completion:nil];
            identifier = @"logOut";
            break;
    }
        return  identifier;
}

-(void)configureLeftMenuButton:(UIButton *)button{
    CGRect frame = button.frame;
    frame.origin = (CGPoint){0,0};
    frame.size = (CGSize){40,40};
    button.frame = frame;
    [button setImage:[UIImage imageNamed:@"üçlü-buton.png"] forState:UIControlStateNormal];
}

-(CGFloat)leftMenuWidth{
    return 250;
    
}



@end
