//
//  NewInvoiceVC.h
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"

@interface NewInvoiceVC : GOViewController

- (IBAction)buttonTakePhoto:(id)sender;

@property(nonatomic,weak)IBOutlet UICollectionView *invoiceCollectionView;

@property(strong,nonatomic) NSMutableArray *invoicePhotoArray;

- (IBAction)deleteButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *deleteButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *saveButtonOutlet;

@property (strong, nonatomic) IBOutlet UITextField *txtFaturaAdi;

@property (strong, nonatomic) IBOutlet UITextField *txtFaturaNo;
@property (strong, nonatomic) IBOutlet UITextField *txtSonOdemeTarihi;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIImageView *invoiceCollectionViewBackgroundImage;

- (IBAction)firstResponderResignationTapped:(id)sender;



@end
