//
//  imageVC.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 23/03/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"

@interface imageVC : GOViewController
@property (strong, nonatomic) IBOutlet UIImageView *invoicePhotoImageView;
@property(strong,nonatomic)UIImage *image;


@property (strong, nonatomic) IBOutlet UIScrollView *imageScrollView;


- (IBAction)closeButtonTapped:(id)sender;
@end
