//
//  imageVC.m
//  GarantiOtel
//
//  Created by volkan.terzioglu on 23/03/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "imageVC.h"

@interface imageVC ()<UIScrollViewDelegate,UIScrollViewAccessibilityDelegate>

@end

@implementation imageVC
@synthesize invoicePhotoImageView,image;

- (void)viewDidLoad {
    [super viewDidLoad];
#pragma mark scrollView Zoom in- out
    
    self.invoicePhotoImageView.image = self.image;
//    self.invoicePhotoImageView.frame = CGRectMake(0, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height);
    self.invoicePhotoImageView.contentMode = UIViewContentModeScaleToFill;

    self.invoicePhotoImageView.frame = CGRectMake(0, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height);
    
    self.imageScrollView.delegate = self;
    self.imageScrollView.backgroundColor = [UIColor blackColor];
    self.view.backgroundColor =[UIColor blackColor];
    self.imageScrollView.bounces = NO;
    CGFloat xZoomScale = self.imageScrollView.bounds.size.width / self.invoicePhotoImageView.bounds.size.width;
    CGFloat yZoomScale = self.imageScrollView.bounds.size.height / self.invoicePhotoImageView.bounds.size.height;
    
    CGFloat minZoomScale = MIN(xZoomScale, yZoomScale);
    self.imageScrollView.minimumZoomScale = minZoomScale;
    self.imageScrollView.maximumZoomScale = 4.0;
    self.imageScrollView.zoomScale = 1.0;
    
//    self.invoicePhotoImageView.layer.shouldRasterize = YES;
//    self.invoicePhotoImageView.layer.rasterizationScale = 2;
    [self.invoicePhotoImageView.layer setMinificationFilter:kCAFilterTrilinear];
    
    #pragma mark TapGesture
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
}

-(void)handleTapGesture:(UITapGestureRecognizer *)sender{
    if(sender.state == UIGestureRecognizerStateRecognized){
        self.imageScrollView.zoomScale = 1.0;
    }
        
}


-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return  self.invoicePhotoImageView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)closeButtonTapped:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    
}
@end
