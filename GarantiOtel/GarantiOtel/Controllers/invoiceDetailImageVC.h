//
//  invoiceDetailImageVC.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 24/03/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"

@interface invoiceDetailImageVC : GOViewController
@property (strong, nonatomic) IBOutlet UIImageView *invoiceDetailImageImageview;


@property (strong, nonatomic) IBOutlet UIScrollView *invoiceDetailImageScrollView;

- (IBAction)closeButtonTapped:(id)sender;

@property(strong,nonatomic)UIImage *image;
@end
