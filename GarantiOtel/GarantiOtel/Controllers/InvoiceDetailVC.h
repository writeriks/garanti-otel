//
//  InvoicesDetailVC.h
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"
#import "Invoice.h"

@interface InvoiceDetailVC : GOViewController

@property (nonatomic, strong) Invoice * inv;


@property (strong, nonatomic) IBOutlet UITextField *invoiceNo;
@property (strong, nonatomic) IBOutlet UITextField *invoiceFirmName;
@property (strong, nonatomic) IBOutlet UITextField *invoiceAmount;

- (IBAction)editInvoiceNoButtonTapped:(id)sender;
- (IBAction)editInvoiceFirmNameButtonTapped:(id)sender;
- (IBAction)editInvoiceAmountButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lblInvoiceNo;
@property (strong, nonatomic) IBOutlet UILabel *lblInvoiceFirmName;
@property (strong, nonatomic) IBOutlet UILabel *lblInvoiceAmount;

- (IBAction)saveButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnEditInvoiceOutlet;

@property (strong, nonatomic) IBOutlet UICollectionView *invoiceDetailCV;


@property (strong,nonatomic) NSMutableArray* invoiceDetailPhotoArray;
- (IBAction)deleteButtonTapped:(UIButton *)sender;

- (IBAction)addImageButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

- (IBAction)resignFirstResponderTapped:(id)sender;



@end
