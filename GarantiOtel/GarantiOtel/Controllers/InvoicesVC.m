//
//  InvoicesVC.m
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "InvoicesVC.h"
#import "GOService.h"
#import "InvoiceDetailVC.h"
#import "NewInvoiceVC.h"
#import "MBProgressHUD.h"
#import "Reachability.h"

@interface InvoicesVC ()<UITableViewDataSource,UITableViewDelegate>{
    MBProgressHUD *HUD;
    
    UIRefreshControl *refreshControl;
    
    Reachability *reachabilityTest;
}

@property(nonatomic) NSMutableArray * invoices;
@property(nonatomic) NSMutableArray * filteredInvoices;
@property(strong, nonatomic) IBOutlet UITableView *tableview;
@end

@implementation InvoicesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testConnection];
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl setTintColor:[UIColor blackColor]];
    [refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
    [self.tableview addSubview:refreshControl];
    [self designVC];
    ///HUD
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.labelText = @"Lütfen Bekleyin.";
    HUD.detailsLabelText = @"Faturalar getiriliyor.";
    [self.view addSubview:HUD];
    HUD.animationType = MBProgressHUDAnimationZoomOut;
    [HUD show:YES];
    [self getInvoices];
    /////
}

-(void)testConnection{

    NSURL *url = [NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/GenelBankaHesaplari"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSHTTPURLResponse *response = nil;
    NSError *error = [[NSError alloc] init];
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    if (response.statusCode == 200) {
        NSLog(@"Connection Established... YAYY!!");
    }
    if (response.statusCode == 404) {
        NSLog(@"No connection");
        self.tableview.hidden=YES;
    }
    if (response ==nil) {
        NSLog(@"Internet bağlantısı çevrimdışı gözüküyor");
    }
//    reachabilityTest = [Reachability reachabilityWithHostName:@"www.google.com"];
//    // Internet is reachable
//    reachabilityTest.reachableBlock = ^(Reachability*reach)
//    {
//        // Update the UI on the main thread
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"Yayyy, we have the interwebs!");
//        });
//    };
//
//    // Internet is not reachable
//    reachabilityTest.unreachableBlock = ^(Reachability*reach)
//    {
//        // Update the UI on the main thread
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"Someone broke the internet :(");
//        });
//    };
//    
//    [reachabilityTest startNotifier];
}

-(void)refreshAction{
    [self testConnection];
    [self getInvoices];
    [refreshControl endRefreshing];
}

-(void)designVC{
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    [self.tableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableview.contentSize = CGSizeMake(self.tableview.frame.size.width, self.tableview.contentSize.height);
    /// TABLEVIEW BACKGROUND IMAGE ATAMA
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
    [tempImageView setFrame:self.tableview.frame];
    self.tableview.backgroundView = tempImageView;
    ///
    
    ////SEARCH HBAR
    self.searchBar.barTintColor = [UIColor orangeColor];
    self.searchBar.translucent = NO;
    self.searchBar.barStyle = UIBarStyleDefault;
}

-(void)getInvoices {
        [[GOService sharedService] getInvoicesOfUser:nil
                                              result:^(NSArray *array, NSError *error) {
                                                  if (array.count != 0 ) {
                                                      self.invoices = array;
                                                      self.filteredInvoices = [NSMutableArray arrayWithCapacity:[self.invoices count]];
                                                      [self.tableview reloadData];
                                                      [HUD hide:YES];
                                                  }
                                                  else
                                                  {
                                                      //                                                  UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Hata" message:@"Fatura bulunamadı" delegate:self cancelButtonTitle:@"Tamam" otherButtonTitles:nil, nil];
                                                      //
                                                      //  alert.show;
                                                      NSLog(@"%@",error);
                                                      NSLog(@"invoice yok");
                                                      [HUD hide:YES];
                                                      // invoice yok
                                                  }
                                              }];
}

#pragma mark - TableView Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.filteredInvoices count];
    }
    else
    {
    return self.invoices.count *2 - 1;//CELL SPACE
    }
}

- (UIImage *)cellBackgroundForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger rowCount = [self tableView:[self tableview] numberOfRowsInSection:0];
    NSInteger rowIndex = indexPath.row;
    UIImage *background = nil;
    if (rowIndex == 0) {
        background = [UIImage imageNamed:@"faturalar-köşeli-buton.png"];
    } else if (rowIndex == rowCount - 1) {
        background = [UIImage imageNamed:@"faturalar-köşeli-buton.png"];
    } else {
        background = [UIImage imageNamed:@"faturalar-köşeli-buton.png"];
    }
    return background;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //CELL SPACE
    static NSString *CELL_ID2 = @"cell2";
    // even rows will be invisible
    if (indexPath.row % 2 == 1)
    {
        UITableViewCell * cell2 = [tableView dequeueReusableCellWithIdentifier:CELL_ID2];
        
        if (cell2 == nil)
        {
            cell2 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:CELL_ID2];
            [cell2.contentView setAlpha:0];
            cell2.backgroundColor = [UIColor clearColor];
            [cell2 setUserInteractionEnabled:NO]; // prevent selection and other stuff
        }
        return cell2;
    }
    //CELL SPACE
    else{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvoiceCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InvoiceCell"];
    }
        Invoice *currentInvoice= nil;
        if (tableView == self.searchDisplayController.searchResultsTableView)
        {
            currentInvoice = [self.filteredInvoices objectAtIndex:[indexPath row]];
        }
        else
        {
            currentInvoice = [self.invoices objectAtIndex:indexPath.row/2];
        }
//    Invoice *currentInvoice = [self.invoices objectAtIndex:indexPath.row/2];//CELL SPACE
    cell.textLabel.text = currentInvoice.bankaSubeAdi;
    UIImage *background = [self cellBackgroundForRowAtIndexPath:indexPath];
    UIImageView *cellBackgroundView = [[UIImageView alloc] initWithImage:background];
    cellBackgroundView.image = background;
    cell.backgroundView = cellBackgroundView;
    cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"disclosure.png"]];
    //cell.detailTextLabel.text = currentInvoice.kayitTarihi;
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return  cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath//CELL SPACE
{
    if (indexPath.row % 2 == 1)
        return 3;
    return 45;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle==UITableViewCellEditingStyleDelete) {
        NSLog(@"DELETE, index : %ld",(long)indexPath.row/2);
        [_invoices removeObjectAtIndex:indexPath.row/2];
        [self.tableview reloadData];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"detailSegue" sender:tableView];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if ([segue.destinationViewController isKindOfClass:[InvoiceDetailVC class]]) {
//        NSIndexPath *indexPath = [self.tableview indexPathForCell:sender];
//        Invoice *currentInvoice = [self.invoices objectAtIndex:indexPath.row/2];
//        InvoiceDetailVC *destination = segue.destinationViewController;
//        destination.inv = currentInvoice;
//    }
//    else if ([segue.destinationViewController isKindOfClass:[NewInvoiceVC class]]) {
//        //
//    }
//
//            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//            NSString *destinationTitle = [[candyArray objectAtIndex:[indexPath row]] name];
//            [candyDetailViewController setTitle:destinationTitle];

    if ( [[segue identifier] isEqualToString:@"detailSegue"] ) {
    
        InvoiceDetailVC *destination = [segue destinationViewController];
        
        // In order to manipulate the destination view controller, another check on which table (search or normal) is displayed is needed
        if(sender == self.searchDisplayController.searchResultsTableView) {
            NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            //            NSString *destinationTitle = [[filteredCandyArray objectAtIndex:[indexPath row]]name];
            Invoice *currentInvoice = [self.filteredInvoices objectAtIndex:[indexPath row]];
            destination.inv = currentInvoice;
        }
        else {
            NSIndexPath *indexPath = [self.tableview indexPathForSelectedRow];
            Invoice *currentInvoice = [self.invoices objectAtIndex:indexPath.row/2];
            destination.inv = currentInvoice;
        }
    }
}

#pragma  mark -searchTableView
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
    
    // Remove all objects from the filtered search array
    [self.filteredInvoices removeAllObjects];
    
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF._bankaSubeAdi contains[c] %@",searchText];
    NSArray *tempArray = [self.invoices filteredArrayUsingPredicate:predicate];
    self.filteredInvoices = [NSMutableArray arrayWithArray:tempArray];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

-(void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller{
    UITableView *tableView = controller.searchResultsTableView;
//    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"  "]];
//    [tempImageView setFrame:self.tableview.frame];
//    tableView.backgroundView = tempImageView;
    tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}



@end




