//
//  SignupVC.m
//  GarantiOtel
//
//  Created by Suleyman Calik on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "SignupVC.h"
#import "GOService.h"

@interface SignupVC ()<UITableViewDelegate>
- (IBAction)cancelButton:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtPasswordAgain;

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
- (IBAction)btnSignUpTapped:(id)sender;

@end

@implementation SignupVC

- (void)viewDidLoad {
    
    self.title = @"Üye ol";
    
    [self.txtName setBackgroundColor:[UIColor clearColor]];
    [self.txtEmail setBackgroundColor:[UIColor clearColor]];
    [self.txtPassword setBackgroundColor:[UIColor clearColor]];
    [self.txtPasswordAgain setBackgroundColor:[UIColor clearColor]];
    [self.btnSignUp setBackgroundColor:[UIColor clearColor]];

    self.txtName.delegate = self;
    self.txtPassword.delegate = self;
    self.txtPasswordAgain.delegate = self;
    self.txtEmail.delegate = self;

    [self signup];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)signup{
//    
//    [[GOService sharedService] signupWithUsername:@""
//                                      andPassword:@""
//                                         andEmail:@""
//                                           result:^(User *user, NSError *error) {
//                                               if (user) {
//                                                   //Giriş Başarılı
//                                               }
//                                               else{
//                                                   // Hata Göster
//                                               }
//                                           }];
    NSString *returnString = [[GOService sharedService] signupWithUsername:@"emir" andPassword:@"emir" andEmail:@"emir@emir.com" andStateId:@"2" andCityID:@"2" andPassword:@"emir" andSurname:@"emir" andNationID:@"2" andMemberGroupId:@"2" andmemberRoleID:@"2" result:nil];
    
    NSLog(returnString);
    
}

- (IBAction)cancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnSignUpTapped:(id)sender {
//    [self rlmSignup];
    [self signUpWithLocalUser];
    
}

-(void)signUpWithLocalUser{
    
    if ([self.txtName.text isEqualToString:@""] || [self.txtPassword.text isEqualToString:@""] || [self.txtPasswordAgain.text isEqualToString:@""] || [self.txtEmail.text isEqualToString:@""] ) {
        NSLog(@"fill all of 'em yo gang");
    }
    else
        [self checkPasswordMatch];
}

-(void)checkPasswordMatch{

    if ([self.txtPassword.text isEqualToString:self.txtPasswordAgain.text]) {
        NSLog(@"yo got the pass bro");
        [self registerUser];
    }
    else
        NSLog(@"dont make me crazy! fill the password correct");

}

-(void)registerUser{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.txtName.text forKey:@"username"];
    [defaults setObject:self.txtPassword.text forKey:@"password"];
    [defaults setObject:self.txtPasswordAgain.text forKey:@"passwordAgain"];
    [defaults setObject:self.txtEmail.text forKey:@"email"];
    [defaults setBool:YES forKey:@"registered"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"toSlider" sender:self];
}

//
//-(void)rlmSignup{
//
//    User *user = [[User alloc]init];
//    user.username = [self.txtName text];
//    user.email = [self.txtEmail text];
//    user.password = [self.txtPassword text];
//    user.passwordAgain = [self.txtPasswordAgain text];
//    
//    RLMRealm *realm = [RLMRealm defaultRealm];
//    
//    [realm beginWriteTransaction];
//    [realm  addObject:user];
//    [realm commitWriteTransaction];
//    
//
//    
//    
//}





@end
