//
//  invoiceDetailImageVC.m
//  GarantiOtel
//
//  Created by volkan.terzioglu on 24/03/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "invoiceDetailImageVC.h"

@interface invoiceDetailImageVC ()<UIScrollViewAccessibilityDelegate, UIScrollViewDelegate>

@end

@implementation invoiceDetailImageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.invoiceDetailImageImageview.image = self.image;
    self.invoiceDetailImageImageview.frame = CGRectMake(0, 0, self.invoiceDetailImageScrollView.frame.size.width, self.invoiceDetailImageScrollView.frame.size.height);
    self.invoiceDetailImageImageview.contentMode = UIViewContentModeScaleToFill;
    self.invoiceDetailImageImageview.transform =CGAffineTransformMakeScale(0.5, 0.5);
    
    self.invoiceDetailImageScrollView.delegate = self;
    self.invoiceDetailImageScrollView.backgroundColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor blackColor];
    self.invoiceDetailImageScrollView.bounces = NO;
    CGFloat xZoomScale = self.invoiceDetailImageScrollView.bounds.size.width / self.invoiceDetailImageImageview.bounds.size.width;
    
    CGFloat yZoomScale = self.invoiceDetailImageScrollView.bounds.size.height / self.invoiceDetailImageImageview.bounds.size.height;
    
    CGFloat minZoomScale = MIN(xZoomScale, yZoomScale);

    self.invoiceDetailImageScrollView.zoomScale = 1.0;
    self.invoiceDetailImageScrollView.minimumZoomScale = minZoomScale;
    self.invoiceDetailImageScrollView.maximumZoomScale = 4.0;


   [self.invoiceDetailImageImageview.layer setMinificationFilter:kCAFilterTrilinear];
    
    #pragma mark TapGesture
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
   }


-(void)handleTapGesture:(UITapGestureRecognizer *)sender{
    if(sender.state == UIGestureRecognizerStateRecognized){
        self.invoiceDetailImageScrollView.zoomScale = 1.0;
    }
}


-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return  self.invoiceDetailImageImageview;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)closeButtonTapped:(id)sender {
//    [self.invoiceDetailImageScrollView setScrollEnabled:NO];
//    [self dismissViewControllerAnimated:YES completion:nil];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}
@end
