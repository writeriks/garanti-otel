//
//  MyProfile.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 12/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"

@interface MyProfile : GOViewController
@property (strong, nonatomic) IBOutlet UIImageView *imgProfilePic;

- (IBAction)btnChangeProfilePic:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtNameSurname;
@property (strong, nonatomic) IBOutlet UILabel *lblNameSurname;

@property (strong, nonatomic) IBOutlet UITextField *txtMail;
@property (strong, nonatomic) IBOutlet UILabel *lblMail;
- (IBAction)editNameSurnameButtonTapped:(id)sender;

- (IBAction)editMailButtonTapped:(id)sender;

- (IBAction)saveProfileButtonTapped:(id)sender;

- (IBAction)editProfilePic:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)resignFirstResponderTapped:(id)sender;
@end
