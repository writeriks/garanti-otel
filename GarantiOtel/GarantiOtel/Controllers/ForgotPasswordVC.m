//
//  ForgotPasswordVC.m
//  GarantiOtel
//
//  Created by Suleyman Calik on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "GOService.h"
@interface ForgotPasswordVC ()
@property (strong, nonatomic) IBOutlet UIImageView *image;

@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.image.layer.cornerRadius = self.image.frame.size.height / 2 ;
    self.image.layer.masksToBounds = YES;
    self.image.layer.borderWidth = 0;
    
    NSData *imageData =  [[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:@"https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xap1/v/t1.0-9/1395178_10152293284647995_461500828_n.jpg?oh=594b1ac234f3030b42cf29a235057aed&oe=557D4E72&__gda__=1433594410_7140ae20e4f193d741b85c5a80af9422"]];
    self.image.image = [UIImage imageWithData:imageData];
}

@end
