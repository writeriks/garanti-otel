//
//  LoginVC.m
//  GarantiOtel
//
//  Created by Suleyman Calik on 01/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "LoginVC.h"
#import "GOService.h"
#import "MBProgressHUD.h"

@interface LoginVC ()<UITableViewDelegate>
{
MBProgressHUD * HUD ;
}
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)btnSignIn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSignIn2;

@end

@implementation LoginVC
- (void)viewDidLoad {

    [self.txtUsername setBackgroundColor:[UIColor clearColor]];
    [self.txtPassword setBackgroundColor:[UIColor clearColor]];
    [self.btnSignIn2  setBackgroundColor:[UIColor clearColor]];
    
    self.txtUsername.delegate = self;
    self.txtPassword.delegate = self;

    //[self userExist];
    
    
    [super viewDidLoad];
}
-(void)userExist{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if (![defaults boolForKey:@"registered"]) {
        NSLog(@"It is not a toy ya GangsTa playing with");
        
    }
    else{
        NSLog(@"EVERYBODY GETS DA PAArTY!!");
        self.txtUsername.text = [defaults objectForKey:@"email"];
        self.txtPassword.text = [defaults objectForKey:@"password"];
        [self performSegueWithIdentifier:@"login" sender:self];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)login {
    
    [[GOService sharedService] loginWithUsername:self.txtUsername.text
                                     andPassword:self.txtPassword.text
                                          result:^(User *user, NSError *error) {
                                              if (user) {
                                                  
                                                  //user'i local'e kaydet
                                                  [self performSegueWithIdentifier:@"login" sender:self];
                                              }
                                              else {
                                                  // hata goster
                                                  
                                                  
                                              }
                                          }];
}

//
//-(void)rlmDeneme{/// get user   email = 'emirhaktan@gmail.com'
//    
//    NSString *email = self.txtUsername.text;
//    NSString *pass = self.txtPassword.text;
//    
//    RLMResults *kullanici = [User objectsWhere:@"email = %@ AND password = %@",email,pass];
//    
//    
//    if (kullanici != Nil) {
//        NSLog(@"%@",kullanici);
//    }
//    
//    
//    
//    }

- (IBAction)loginButtonTapped:(UIButton *)sender {
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.animationType = MBProgressHUDAnimationZoomIn;
    [HUD show:YES];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([self.txtUsername.text isEqualToString:[defaults objectForKey:@"email"]] && [self.txtPassword.text isEqualToString:[defaults objectForKey:@"password"]]) {
        NSLog(@"Congrats bro! got 'cha");
        [HUD hide:YES];
        [self performSegueWithIdentifier:@"login" sender:self];
    }
    else
        NSLog(@"something is missing here");
        [HUD hide:YES];
    //[self rlmDeneme];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
