//
//  InvoicePost.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 14/04/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InvoicePost : NSObject
@property(nonatomic) NSString * faturaNo;
@property(nonatomic) NSString * faturaTarihi;
@property(nonatomic) NSString * faturaTutari;
@property(nonatomic) NSString * ID;
@property(nonatomic) NSString * kategoriID;
@property(nonatomic) NSString * kayitTarihi;
@property(nonatomic) NSString * onay;
@property(nonatomic) NSString * saticiFirma;
@property(nonatomic) NSString * uyeID;

@end
