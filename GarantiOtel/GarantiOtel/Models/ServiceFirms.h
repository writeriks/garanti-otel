//
//  ServiceFirms.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 27/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOViewController.h"

@interface ServiceFirms : GOViewController

@property (nonatomic) NSInteger      ID;
@property (nonatomic) NSString      *firmaAdi;
@property (nonatomic) NSString      *firmaAdresi;
@property (nonatomic) NSString      *firmaEmaili;
@property (nonatomic) NSString      *firmaTelefonu1;
@property (nonatomic) NSString      *firmaTelefonu2;
@property (nonatomic) NSString      *firmaTelefonu3;
@property (nonatomic) NSString      *firmaUrl;
@property (nonatomic) NSInteger     ilceID;
@property (nonatomic) NSString      *kayitTarihi;
@property (nonatomic) BOOL          onay;
@property (nonatomic) NSInteger     sehirID;
@property (nonatomic) NSString      *yetkiliAdi;
@property (nonatomic) NSString      *yetkiliSoyadi;

@end
