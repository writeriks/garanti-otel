//
//  GOService.h
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 07/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Invoice.h"
#import "FAQ.h"
#import "ServiceFirms.h"
typedef void (^UserBlock) (User * user , NSError * error);
typedef void (^ArrayBlock)(NSArray * array, NSError * error);
typedef void (^InvoiceBlock)(Invoice * invoice, NSError * error);



@interface GOService : NSObject


+(GOService *)sharedService;

-(void)loginWithUsername:(NSString *)userName
             andPassword:(NSString *)password
                  result:(UserBlock)result;

-(NSString*)signupWithUsername:(NSString *)userName
              andPassword:(NSString *)gender
                 andEmail:(NSString *)email
                 andStateId:(NSString *)stateID
                 andCityID:(NSString *)cityID
              andPassword:(NSString *)password
                 andSurname:(NSString *)surname
                 andNationID:(NSString *)nationID
                 andMemberGroupId:(NSString *)memberGroupID
                 andmemberRoleID:(NSString *)memberRoleID
                   result:(UserBlock)result;

-(void)getInvoicesOfUser:(User *)user
                  result:(ArrayBlock)result;

-(NSString *)newInvoiceWithUser:(User *)user
                    andID:(NSString *)iD
             andInvoiceNo:(NSString *)invoiceNo
            andDealerFirm:(NSString *)dealerFirm
              andMemberID:(NSString *)memberID
         andInvoiceAmount:(NSString *)invoiceAmount
           andInvoiceDate:(NSString *)invoiceDate
            andCategoryID:(NSString *)categoryID
          andRegistryDate:(NSString *)registryDate
            andPermission:(NSString *)permission;


-(NSString *)newInvoiceWithImageArray:(NSMutableArray *)imageArray
                          andID:(NSString *)iD
                 andDescription:(NSString *)description
                    andFileName:(NSString *)fileName
                  andFileLength:(NSString *)fileLength
                    andFileType:(NSString *)fileType
               andFileExtention:(NSString *)fileExtention
                  andRegistryID:(NSString *)registryID
                andRegistryDate:(NSString *)registryDate
                  andPermission:(NSString *)permission
                  andPersonalID:(NSString *)personalID
                     andProfile:(NSString *)profile
                    andSequence:(NSString *)squence
                   andTableName:(NSString *)tableName;



-(void)editInvoiceWithUser:(User *)user
            andInvoiceName:(NSString *)invoiceName
           andInvoiceImage:(UIImage *)invoiceImage
                    result:(InvoiceBlock)result;

-(void)forgorPasswordWithEmail:(NSString *)email
                        result:(UserBlock) result;


-(void)getFAQOfUser:(User *)user
             result:(ArrayBlock)result;


-(void)getServisFirmsOfUser:(User *)User
                     result:(ArrayBlock)result;


@end
