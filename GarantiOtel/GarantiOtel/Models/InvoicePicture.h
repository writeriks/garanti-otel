//
//  InvoicePicture.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 25/03/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InvoicePicture : NSObject



@property(nonatomic) NSString   *aciklama;
@property(nonatomic) NSString   *dosyaAdi;
@property(nonatomic) NSString   *dosyaBoyutu;
@property(nonatomic) NSString   *dosyaTipi;
@property(nonatomic) NSString   *dosyaUzantisi;
@property(nonatomic) NSString   *ID;
@property(nonatomic) NSString   *kayitID;
@property(nonatomic) NSString   *kayitZamani;
@property(nonatomic) NSString   *onay;
@property(nonatomic) NSString   *personelID;
@property(nonatomic) NSString   *profil;
@property(nonatomic) NSString   *sirasi;
@property(nonatomic) NSString   *tabloAdi;

@end
