
//
//  GOService.m
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 07/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import "GOService.h"


static GOService * _service;

@implementation GOService


+(GOService *)sharedService
{
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        _service = [[GOService alloc] init];
    });
    
    return _service;
}

-(void)loginWithUsername:(NSString *)userName
             andPassword:(NSString *)password
                  result:(UserBlock)result
{
    // server cagrisini yap
    // sonucu result ile don
}

-(NSString *)signupWithUsername:(NSString *)userName andPassword:(NSString *)gender andEmail:(NSString *)email andStateId:(NSString *)stateID andCityID:(NSString *)cityID andPassword:(NSString *)password andSurname:(NSString *)surname andNationID:(NSString *)nationID andMemberGroupId:(NSString *)memberGroupID andmemberRoleID:(NSString *)memberRoleID result:(UserBlock)result
{
    // server cagrisini yap
    // sonucu result ile don
    NSArray *keys = [NSArray arrayWithObjects:@"Ad",@"Cinsiyet",@"Email",@"IlceID",@"SehirID",@"Sifre",@"Soyad",@"UlkeID",@"UyeGrupID",@"UyeRoleID", nil];
    NSArray *objects = [NSArray arrayWithObjects:userName,gender,email,stateID,cityID,password,surname,nationID,memberGroupID,memberRoleID, nil];
    
    NSData *_jsonData=nil;
    NSString *_jsonString=nil;
    NSURL *url2=[NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/UyeKaydet"];
    NSDictionary *JsonDictionary=[NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    if([NSJSONSerialization isValidJSONObject:JsonDictionary]){
        _jsonData=[NSJSONSerialization dataWithJSONObject:JsonDictionary options:0 error:nil];
        _jsonString=[[NSString alloc]initWithData:_jsonData encoding:NSUTF8StringEncoding];
    }
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url2];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:_jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[_jsonData length]] forHTTPHeaderField:@"Content-Length"];
    NSHTTPURLResponse *response = nil;
    NSError *error = [[NSError alloc] init];
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", data);
    if (data) {
        return data;
    }
    else
        return error;
    
    
}

-(void)forgorPasswordWithEmail:(NSString *)email result:(UserBlock)result
{
    //server çağrısını yap
    // sonucu result ile dön
}

-(void)getFAQOfUser:(User *)user
             result:(ArrayBlock)result
{
//    NSURL *url = [NSURL URLWithString:@"http://www.bulbulustur.com/YardimServisi.svc/GenelYardimSikSorulanSorular"];
//    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
//    request.HTTPMethod = @"GET";
////    request.HTTPBody = [@"username=bidi" dataUsingEncoding:NSUTF8StringEncoding];
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        if (data) {
//            NSDictionary *responsDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
//            NSArray *liste = responsDict[@"TumListeResult"];
//            
//            NSMutableArray *faqs =[NSMutableArray array];
//            
//            
//            for (NSDictionary *faqDictionary in liste) {
//                FAQ * faq = [[FAQ alloc] init];
//                
//                for (NSString *key in faqDictionary) {
//                    id value = faqDictionary[key];
//                    if (![value isKindOfClass:[NSNull class]]) {
//                        [faq setValue:value forKey:key];
//                    }
//                }
//                [faqs addObject:faq];
//            }
//            if (result) {
//                result(faqs,nil);
//            }
//        }
//            else{
//                NSLog(@"Hata %@",connectionError);
//                if (result) {
//                    result(nil,connectionError);
//                }
//            }
//        
//    }];
    
}


-(void)getServisFirmsOfUser:(User *)User
                     result:(ArrayBlock)result{
    NSURL * url = [NSURL URLWithString:@"http://www.bulbulustur.com/ServiceFirmalariServisi.svc/GarantiOtelServisFirmalari"];
    NSURLRequest *reqest = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:reqest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments
                                                                           error:nil];
            NSArray *liste = responseDict[@"TumListeResult"];
            NSMutableArray *serviceFirms=[NSMutableArray array];
            
            for (NSDictionary *serviceFirmsDict in liste) {
                ServiceFirms *sfirms =[[ServiceFirms alloc]init];

                for (NSString *key in serviceFirmsDict) {
                    id value = serviceFirmsDict[key];
                    if (![value isKindOfClass:[NSNull class]]) {
                    [sfirms setValue:value forKey:key];
                    }
                }
                [serviceFirms addObject:sfirms];
            }
            
            if (result) {
                result(serviceFirms,nil);
            }
            
            if (connectionError) {
                NSLog(@"Hata %@", connectionError);
            }
        }
        else{
        NSLog(@"Hata %@",connectionError);
            if (result) {
                result(nil,connectionError);
            }
        }
    }];
    
}


-(void)getInvoicesOfUser:(User *)user
                  result:(ArrayBlock)result
{
//    NSURL * url = [NSURL URLWithString:@"http://www.bulbulustur.com/FaturaServisi.svc/GarantiOtelFaturalar"];
//    NSURLRequest * request = [NSURLRequest requestWithURL:url];
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        if (data) {
//            NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];//en baştaki süslü parantez bize key value değerli bir dictionary olduğunu söyledi
//            NSArray * liste = responseDict[@"TumListeResult"];//response dict içinde 2 elemanlı bir array var bu yüzden array tanımlamalı
//            
//            NSMutableArray * invoices = [NSMutableArray array]; //bu array içindeki veriler bir arrayde tutularak gösterilecek. bu yüzden bu array tanımlandı
//            for (NSDictionary * invoiceDict in liste) {
//                Invoice * invoice = [[Invoice alloc] init];
//                
//                for (NSString * key in invoiceDict) {
//                    [invoice setValue:invoiceDict[key] forKey:key];
//                }
//                
//                [invoices addObject:invoice];
//            }
//            
//            if (result) {
//                result(invoices,nil);
//            }
//            
//        }
//        else {
//            NSLog(@"HATA %@",connectionError);
//            if (result) {
//                result(nil,connectionError);
//            }
//
//        }
//    }];
    
    NSURL *url = [NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/GenelBankaHesaplari"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            NSArray *liste = responseDict[@"GenelBankaHesaplariResult"];
            NSMutableArray *bankAcc = [NSMutableArray array];

            for (NSDictionary *bankAccountDictionary in liste) {
                Invoice *invoice = [[Invoice alloc]init];
                for (NSString *key in bankAccountDictionary) {
                    //[invoice setValue:bankAccountDictionary[key] forKey:key];
                    id value = bankAccountDictionary[key];
                    if (![value isKindOfClass:[NSNull class]]) {
                        [invoice setValue:value forKey:key];
                    }
                }
                [bankAcc addObject:invoice];
            }
            if (result) {
                result(bankAcc,nil);
            }
        }
        else{
            NSLog(@"Hata %@",connectionError);
            if (result) {
                result(nil,connectionError);
            }
        }
    }];
}

-(NSString *)newInvoiceWithUser:(User *)user
                    andID:(NSString *)iD
             andInvoiceNo:(NSString *)invoiceNo
            andDealerFirm:(NSString *)dealerFirm
              andMemberID:(NSString *)memberID
         andInvoiceAmount:(NSString *)invoiceAmount
           andInvoiceDate:(NSString *)invoiceDate
            andCategoryID:(NSString *)categoryID
          andRegistryDate:(NSString *)registryDate
            andPermission:(NSString *)permission
{
        NSArray *keys = [NSArray arrayWithObjects:@"faturaNo",@"faturaTarihi",@"faturaTutari",@"ID",@"kategoriID",@"kayitTarihi",@"onay",@"saticiFirma",@"uyeID" , nil];
    NSArray *objects = [NSArray arrayWithObjects:invoiceNo,invoiceDate,invoiceAmount,iD,categoryID,registryDate,permission,dealerFirm,memberID, nil];
    
    NSData *_jsonData=nil;
    NSString *_jsonString=nil;
    NSURL *url2=[NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/UyeFaturaKaydet"];
    NSDictionary *JsonDictionary=[NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    if([NSJSONSerialization isValidJSONObject:JsonDictionary]){
        _jsonData=[NSJSONSerialization dataWithJSONObject:JsonDictionary options:0 error:nil];
        _jsonString=[[NSString alloc]initWithData:_jsonData encoding:NSUTF8StringEncoding];
    }
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url2];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:_jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[_jsonData length]] forHTTPHeaderField:@"Content-Length"];
    NSHTTPURLResponse *response = nil;
    NSError *error = [[NSError alloc] init];
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", data);
    if (data) {
        return data;
    }
    else
        return error;
}

-(NSString *)newInvoiceWithImageArray:(NSMutableArray *)imageArray
                          andID:(NSString *)iD
                 andDescription:(NSString *)description
                    andFileName:(NSString *)fileName
                  andFileLength:(NSString *)fileLength
                    andFileType:(NSString *)fileType
               andFileExtention:(NSString *)fileExtention
                  andRegistryID:(NSString *)registryID
                andRegistryDate:(NSString *)registryDate
                  andPermission:(NSString *)permission
                  andPersonalID:(NSString *)personalID
                     andProfile:(NSString *)profile
                    andSequence:(NSString *)squence
                   andTableName:(NSString *)tableName
{
    //FIRST JSON
    NSArray *keyForInvPic = [NSArray arrayWithObjects:@"aciklama",@"dosyaAdi",@"dosyaBoyutu",@"dosyaTipi",@"dosyaUzantisi",@"ID",@"kayitID",@"kayitZamani",@"onay",@"personelID",@"profil",@"sirasi",@"tabloAdi",nil];
    NSArray *objectForInvPic = [NSArray arrayWithObjects:description,fileName,fileLength,fileType,fileExtention,iD,registryID,registryDate,permission,personalID,profile,squence,tableName,nil];
    NSData *_jsonData2=nil;
    NSString *_jsonString2=nil;
    NSDictionary *JsonDictionary2=[NSDictionary dictionaryWithObjects:objectForInvPic forKeys:keyForInvPic];
    if([NSJSONSerialization isValidJSONObject:JsonDictionary2]){
        _jsonData2=[NSJSONSerialization dataWithJSONObject:JsonDictionary2 options:0 error:nil];
        _jsonString2=[[NSString alloc]initWithData:_jsonData2 encoding:NSUTF8StringEncoding];
        
    }
    //SECOND JSON
    NSArray *keys=[NSArray arrayWithObjects:@"faturaResim",@"dosya",nil];
    NSArray *objects=[NSArray arrayWithObjects:JsonDictionary2,imageArray,nil];
    NSData *_jsonData=nil;
    NSString *_jsonString=nil;
    NSDictionary *JsonDictionary=[NSDictionary dictionaryWithObjects:objects forKeys:keys];
    if([NSJSONSerialization isValidJSONObject:JsonDictionary]){
        _jsonData=[NSJSONSerialization dataWithJSONObject:JsonDictionary options:0 error:nil];
        _jsonString=[[NSString alloc]initWithData:_jsonData encoding:NSUTF8StringEncoding];
        
    }
    //CONNECTION
    NSURL *url=[NSURL URLWithString:@"http://ws.tbilgi.com/GarantiService/GarantiOtelService.svc/UyeFaturaResimKaydet"];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:_jsonData];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[_jsonData length]] forHTTPHeaderField:@"Content-Length"];
    NSLog(@"REQUEST  %@",request);
    NSHTTPURLResponse *response = nil;
    NSError *error = [[NSError alloc] init];
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *data=[[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
    if (data) {
        NSLog(@"%@", data);
        return data;
    }
    else
        return error;
}


-(void)editInvoiceWithUser:(User *)user
            andInvoiceName:(NSString *)invoiceName
           andInvoiceImage:(UIImage *)invoiceImage
                    result:(InvoiceBlock)result
{
    // server cagrisini yap
    // sonucu result ile don
}





@end
