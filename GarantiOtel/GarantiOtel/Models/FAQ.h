//
//  FAQ.h
//  GarantiOtel
//
//  Created by volkan.terzioglu on 16/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAQ : NSObject

@property (nonatomic) NSString  *ActionName;
@property (nonatomic) NSString  *ControllerName;
@property (nonatomic) NSInteger  ID;
@property (nonatomic) NSString  *cevap;
@property (nonatomic) BOOL       onay;
@property (nonatomic) NSString  *soru;
@property (nonatomic) NSInteger  yardimAltKategoriID;
@end
