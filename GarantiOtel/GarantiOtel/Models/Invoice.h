//
//  Invoices.h
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 07/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Invoice : NSObject


//@property(nonatomic) NSInteger ID;
//@property(nonatomic) NSString * faturaNo;
//@property(nonatomic) NSInteger faturaTutari;
//@property(nonatomic) NSInteger kategoriID;
//@property(nonatomic) BOOL onay;
//@property(nonatomic) NSString * saticiFirma;
//@property(nonatomic) NSInteger uyeID;
//
//
//@property(nonatomic) NSString * kayitTarihi;
//@property(nonatomic) NSString * faturaTarihi;

@property(nonatomic) NSInteger  ID;
@property(nonatomic) NSString * bankaAdi;
@property(nonatomic) NSString * bankaHesapNo;
@property(nonatomic) NSString * bankaHesapSahibi;
@property(nonatomic) NSString * bankaIBAN;
@property(nonatomic) NSInteger bankaID;
@property(nonatomic) NSString * bankaSubeAdi;
@property(nonatomic) NSString * bankaSubeKodu;
@property(nonatomic) NSString * logo1;
@property(nonatomic) NSString * logo2;
@property(nonatomic) NSString * logo3;
@property(nonatomic) BOOL  onay;
@property(nonatomic) NSString * sehirAdi;
@property(nonatomic) NSInteger  sehirID;
@property(nonatomic) BOOL  sitedeGorunsunmu;

@end
