//
//  User.h
//  GarantiOtel
//
//  Created by Emir haktan Ozturk on 07/02/15.
//  Copyright (c) 2015 Emir haktan Ozturk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RLMObject.h"
#import <Realm/Realm.h>
RLM_ARRAY_TYPE(User)

@interface User : RLMObject

//
//@property(nonatomic) NSString * username;
//@property(nonatomic) NSString * email;
//@property(nonatomic) NSString * password;
//@property(nonatomic) NSString * passwordAgain;
//@property(nonatomic) RLMArray<User> * user2;


@property(nonatomic) NSString * Ad;
@property(nonatomic) NSString * cinsiyet;
@property(nonatomic) NSString * email;
@property(nonatomic) NSString * IlceID;
@property(nonatomic) NSString * SehirID;
@property(nonatomic) NSString * Sifre;
@property(nonatomic) NSString * Soyad;
@property(nonatomic) NSString * UlkeID;
@property(nonatomic) NSString * UyeGrupID;
@property(nonatomic) NSString * UyeRoleID;


@end
